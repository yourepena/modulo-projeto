import { Body, Controller, HttpException, HttpStatus, Post, Get, Query } from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiCreatedResponse,
    ApiImplicitQuery,
    ApiOkResponse,
    ApiOperation,
    ApiUseTags,
} from '@nestjs/swagger';

import { Projeto } from './models/projeto.model';
import { ProjetoService } from './projeto.service';
import { ProjetoVm } from './models/view-models/projeto-vm.model';
import { ApiException } from 'modulo-comum/src/comum/api-exception.model';

import { GetOperationId } from 'modulo-comum/src/utilities/get-operation-id.helper';
import { EnumToArray } from 'modulo-comum/src/utilities/enum-to-array.helper';
import { ProjetoStatus } from './models/projeto-status.enum';
import { isArray, map } from 'lodash';

@Controller('projeto')
@ApiUseTags(Projeto.modelName)
export class ProjetoController {

    constructor(private readonly _projetoService: ProjetoService) { }

    @Post('register')
    @ApiCreatedResponse({ type: ProjetoVm })
    @ApiBadRequestResponse({ type: ApiException })
    @ApiOperation(GetOperationId(Projeto.modelName, 'Register'))
    async register(@Body() vm: ProjetoVm): Promise<ProjetoVm> {
        const { nome } = vm;

        if (!nome) {
            throw new HttpException('Nome do projeto é obrigatório', HttpStatus.BAD_REQUEST);
        }

        let exist;
        try {
            exist = await this._projetoService.findOne({ nome });
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (exist) {
            throw new HttpException(`Um projeto com o nome ${nome} já existe`, HttpStatus.BAD_REQUEST);
        }

        const newUser = await this._projetoService.register(vm);
        return this._projetoService.map<ProjetoVm>(newUser);
    }

    @Get()
    @ApiOkResponse({ type: ProjetoVm, isArray: true })
    @ApiBadRequestResponse({ type: ApiException })
    @ApiOperation(GetOperationId(Projeto.modelName, 'GetAll'))
    @ApiImplicitQuery({ name: 'status', enum: EnumToArray(ProjetoStatus), required: false, isArray: true })
    async get(@Query('status') status?: ProjetoStatus): Promise<ProjetoVm[]> {

        const filter = {
            status : null,
        };

        if (status) {
            filter.status = { $in: isArray(status) ? [...status] : [status] };
        }

        try {
            const projetos = await this._projetoService.findAll(filter);
            return this._projetoService.map<ProjetoVm[]>(map(projetos, projeto => projeto.toJSON()));
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Get('tarefas')
    async getTarefas(): Promise<any[]> {

        try {
            const anys = await this._projetoService.getTarefas();
            return anys;
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
