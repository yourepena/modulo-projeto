import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Projeto, ProjetoModel } from './models/projeto.model';
import { ComumService } from 'modulo-comum/src/comum/comum.service';
import { InjectModel } from '@nestjs/mongoose';
import { MapperService } from './mapper/mapper.service';
import { ModelType } from 'typegoose';
import { ProjetoVm } from './models/view-models/projeto-vm.model';
import { Client, ClientProxy, Transport } from '@nestjs/microservices';

@Injectable()
export class ProjetoService extends ComumService<Projeto> {

    @Client({ transport: Transport.TCP })
    client: ClientProxy;

    constructor(
        @InjectModel(Projeto.modelName) private readonly _projetoModel: ModelType<Projeto>,
        private readonly _mapperService: MapperService,
    ) {
        super();
        this._model = _projetoModel;
        this._mapper = _mapperService.mapper;
    }

    async register(vm: ProjetoVm) {
        const { nome, nomePrograma, nomePortifolio } = vm;

        const newProjeto = new ProjetoModel();
        newProjeto.nome = nome.trim().toLowerCase();
        newProjeto.nomePrograma = nomePrograma;
        newProjeto.nomePortifolio = nomePortifolio;

        try {
            const result = await this.create(newProjeto);
            return result.toJSON() as Projeto;
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public async getTarefas(): Promise<any[]> {
        const response = await this.client.send<any[]>(
          { type: 'get-tarefas' },
          { someImaginaryParams: 42 },
        );
        return response.toPromise();
      }

}
