import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Projeto } from './models/projeto.model';
import { ProjetoService } from './projeto.service';
import { ProjetoController } from './projeto.controller';
import { MapperService } from './mapper/mapper.service';

@Module({
    imports: [MongooseModule.forFeature([{ name: Projeto.modelName, schema: Projeto.model.schema }])],
    providers: [MapperService, ProjetoService],
    controllers: [ProjetoController],
    exports: [ProjetoService, MapperService],
})
export class ProjetoModule {
}