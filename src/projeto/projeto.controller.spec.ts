import { Test, TestingModule } from '@nestjs/testing';
import { ProjetoController } from './projeto.controller';

describe('Projeto Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ProjetoController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: ProjetoController = module.get<ProjetoController>(ProjetoController);
    expect(controller).toBeDefined();
  });
});
