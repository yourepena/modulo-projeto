import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { ComumModelVm } from 'modulo-comum/src/comum/comum.model';

export class ProjetoVm extends ComumModelVm {
    @ApiModelProperty() nome: string;
    @ApiModelPropertyOptional() nomePrograma?: string;
    @ApiModelPropertyOptional() nomePortifolio?: string;
    @ApiModelPropertyOptional() status?: string;
}
