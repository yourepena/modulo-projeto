import { ModelType, pre, prop, Typegoose } from 'typegoose';
import { schemaOptions } from 'modulo-comum/src/comum/comum.model';
import { ProjetoStatus } from './projeto-status.enum';

@pre<Projeto>('findOneAndUpdate', function(next) {
    this._update.updatedAt = new Date(Date.now());
    next();
})
export class Projeto extends Typegoose {
    @prop({
        required: [true, 'Nome do projeto é obrigatório'],
        unique: true,
        minlength: [5, 'O nome do Projeto deve conter mais de 5 caracteres'],
    })
    nome: string;

    @prop() nomePrograma?: string;

    @prop() nomePortifolio?: string;

    @prop({ default: Date.now() })
    createdAt?: Date;

    @prop({ default: Date.now() })
    updatedAt?: Date;

    @prop({ enum: ProjetoStatus, default: ProjetoStatus.EmDia })

    @prop() _id: string;

    static get model(): ModelType<Projeto> {
        return new Projeto().getModelForClass(Projeto, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
}

export const ProjetoModel = new Projeto().getModelForClass(Projeto, { schemaOptions });
