export enum ProjetoStatus {
    Atrasado = 'Atrasado',
    EmDia = 'Em dia',
    Concluido = 'Concluído',
}
